const   express = require('express'),
        app = express(),
        port = 3000,
        config = require('./public/jsonData.json');

app.use(express.static('public'));

app.get('/', (req, res) => res.send('Hello World!'))

app.get('/data', (req, res) => { 
    console.log('Data req')
    res.json({name: 'MayurGG'})
})
app.get('/date', (req, res) => { 
    console.log('Date req')
    res.json({name: 'MMGalande'})
})

app.get('/config', (req, res) => {
    res.json(config);
})

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))