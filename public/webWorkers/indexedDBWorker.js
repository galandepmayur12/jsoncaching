console.log('inside indexedDBWorker')

var checkObjectStore = function(name, databaseStoreList) {
    for (var store in databaseStoreList) {
        if (databaseStoreList[store] === name) {
            return true;
            
        }
        return false;
    }
}

var cacheData = {
    "version": "1",
    "URLs": [
        {
            "version": "1", 
            "url": "http://localhost:3000/data"
        }
    ]

}

var reply = function() {
    if(arguments.length >= 1) {
        postMessage({ taskListener: arguments[0],  taskListenerArguments: Array.prototype.slice.call(arguments, 1)})
    }
}

var indexedDBApi = {
    getUrlData: function(url){
        reply('jsonData', { name: 'mayur', id: '1770' })
    },
    updateCacheData: function(cacheNeedToBeUpdated) {
        if(cacheNeedToBeUpdated) {
            var request = indexedDB.open('cacheStore');
            request.onupgradeneeded = function() {
                
                    
                    // index = objectStore.index()
            }
            request.onsuccess = function(event) {
                var database = event.target.result,
                    objectStore = database.transaction('jsonCache', 'readwrite').objectStore('jsonCache'),
                    dataReq = objectStore.get('dataCache');
                    objectStore.openCursor().onsuccess = function(event) {
                        var cursor = event.target.result;
                        if(cursor) {
                            console.log('cursor');
                            var data = cursor.value,
                                requestAPI = new XMLHttpRequest();

                            requestAPI.open('GET', data.url)
                            requestAPI.onreadystatechange = function() {
                                if(requestAPI.readyState === XMLHttpRequest.DONE && requestAPI.status === 200) {
                                    var objectStore = database.transaction('jsonCache', 'readwrite').objectStore('jsonCache'),
                                        requestedData = JSON.parse(requestAPI.responseText);
                                    data.data = requestedData;
                                    updateRequest = objectStore.put(data)
                                    updateRequest.onsuccess = function(event) {
                                        var data = event.target.result;
                                        console.log('das', data)
                                    }
                                }
                            }
                            requestAPI.send();
                            cursor.continue();
                        }
                    }
                    /*objectStore.getAll().onsuccess = function(event) {
                        var jsonCacheData = event.target.result;
                        jsonCacheData.forEach(function(urlData) {
                            var data = urlData,
                                requestAPI = new XMLHttpRequest();

                            requestAPI.open('GET', data.url)
                            requestAPI.onreadystatechange = function() {
                                if(requestAPI.readyState === XMLHttpRequest.DONE && requestAPI.status === 200) {
                                    var objectStore = database.transaction('jsonCache', 'readwrite').objectStore('jsonCache'),
                                        requestedData = JSON.parse(requestAPI.responseText);
                                    data.data = requestedData;
                                    updateRequest = objectStore.put(data)
                                    updateRequest.onsuccess = function(event) {
                                        var data = event.target.result;
                                        console.log('das', data)
                                    }
                                }
                            }
                            requestAPI.send();
                        });
                        
                    }*/
                    // objectStore.openCursor().onsuccess = function(event) {
                    //     var cursor = event.target.result;
                    //     if(cursor) {
                            
                    //         cursor.continue();
                    //     }
                    // }
                    dataReq.onsuccess = function(event) {
                        // var data = event.target.result;
                        // data.data = {attr: 'hello'}
                        // updateRequest = objectStore.put(data)
                        // updateRequest.onsuccess = function(event) {
                        //     var data = event.target.result;
                        //     console.log('das', data)
                        // }
                    }
                // var database = event.target.result,
                //     transaction = database.transaction('jsonCache'),
                //     objectStore =   transaction.objectStore('jsonCache'),
                //     index = objectStore.index()
            }
        }         
    },
    checkAPIData: function(data) {
        var request = indexedDB.open('cacheStore'),
            isDatabasePopulated = true;
        request.onupgradeneeded = function() {
            isDatabasePopulated = false;
        }
        request.onsuccess = function(event) {
            if(isDatabasePopulated) {
                var database = event.target.result,
                objectStore = database.transaction('jsonCache', 'readwrite').objectStore('jsonCache'),
                dataReq = objectStore.get('dataCache');

                dataReq.onsuccess = function(event) {
                    var apiData = event.target.result;
                    reply('checkAPIData', apiData)
                }
                dataReq.onerror = function() {
                    reply('checkAPIData', false)
                }
                dataReq.onabort = function() {
                    console.log('dsa')
                }   
            }
        }
    },
    dataVersionCheck: function(data) {
        var newDb = false;
        var toBeUpdated = false;
        var request = indexedDB.open('cacheStore', data.version);
        request.onupgradeneeded = function(event) {
            db = event.target.result;
            if( db.objectStoreNames.length === 0 && checkObjectStore('jsonCache', db.objectStoreNames) === false) {
                db.createObjectStore('jsonCache', {keyPath:'name'});
            }
            
            newDb = true;

        }
        request.onsuccess = function(event) {
            if(newDb) {
                var tx = db.transaction('jsonCache', 'readwrite'),
                    jsonCache = tx.objectStore('jsonCache');
                db.onerror = function(e) {
                    console.log('error', e)
                }
                db.onabort = function(e) {
                    console.log(e, 'error')
                }
                data.URLs.forEach(function(urlData) {
                    jsonCache.add(urlData)
                });

                var version = event.target.result.version;
                if(version !== data.version || newDb ||  db.objectStoreNames.length === 0) {
                    toBeUpdated = true
                }
                reply('dataVersionCheck', toBeUpdated);

            }
            
        }
    }
}

onmessage = function(event) {
    if(event.data instanceof Object && event.data.hasOwnProperty('task') && event.data.hasOwnProperty('taskParam')) {
        indexedDBApi[event.data.task].apply(self, event.data.taskParam);
        // postMessage(data)
    }
}