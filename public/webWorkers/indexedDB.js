var IndexedDB = function(databaseName, version) {
    var indexedDBInstance, 
        db;

    

    this.open = function() {
        if(indexedDBInstance == null) {
            indexedDBInstance = indexedDB.open(databaseName)
        }
    
        indexedDBInstance.onupgradeneeded = function(event) {
            db = event.target.result;
        }
    
        indexedDBInstance.onsuccess = function() {
            db = event.target.result;
        }
    }

    this.open();

    this.addObjectStore = function(objectStore) {
        (objectStore.name, {keyPath: objectStore.keyPath});
    }

    this.addObject = function(object) {
        var tx = db.tranaction(object.objectStoreName, 'readwrite'),
            objectStore = tx.objectStore(object.objectStoreName);
            objectStore.add(object.data)
    }

    this.viewObjectStore = function(objectStore){
        var tx = db.tranaction(objectStore.name, 'readonly'),
            objectStoreCollection = tx.objectStore(objectStore.name),
            request = objectStoreCollection.openCursor();

            request.onsuccess = function(event) {
                var cursor = event.target.result;

                if(cursor) {
                    cursor.continue();
                }
            }
    }

    
}