importScripts('../customWorker.js')
var req = new XMLHttpRequest()

req.open("GET",'/config')
req.send();
req.onload = function() {
    json = JSON.parse(req.responseText)
    console.log(json)

    var indexedDBWorker = new CustomWebWorker('./indexedDBWorker.js');
    indexedDBWorker.addListener('dataVersionCheck', function(data) {
        console.log('dataVersionCheck', data)
        if(data) {
            indexedDBWorker.postMessage('updateCacheData', data)
        }
    });
    indexedDBWorker.addListener('jsonData', function(data){
        console.log('jsonDtata', data)
    })
    indexedDBWorker.postMessage('dataVersionCheck', json)
    indexedDBWorker.postMessage('getUrlData', 'localhost:3000')

}
