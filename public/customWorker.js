var CustomWebWorker = function(workerScriptPath) {
    var that = this,
        customWorker,
        listeners = [];

    if(Worker) {
        customWorker = new Worker(workerScriptPath);
    }

    this.terminate = function() {
        customWorker.terminate();
    }

    this.addListener = function(message, listener) {
        listeners[message] = listener;
    }

    this.removeListener = function(message) {
        delete listeners[message];
    }

    this.postMessage = function() {
        if(arguments.length >= 1) {
            customWorker.postMessage({
                "task": arguments[0],
                "taskParam":  Array.prototype.slice.call(arguments, 1)
            })
        }
    }

    customWorker.onmessage = function(event) {
        if(event instanceof Object && event.data.hasOwnProperty('taskListener') && event.data.hasOwnProperty('taskListenerArguments')) {
            listeners[event.data['taskListener']].apply(that, event.data['taskListenerArguments'])
        }
    }
}